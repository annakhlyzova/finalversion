import glob,os,re, sys
from bs4 import BeautifulSoup
import numpy as np
import matplotlib.pyplot as plt
import time
from collections import Counter
import math
import operator
from phraseQuery import PhraseQuery
start_time = time.time()


'''The parser outputs the parsed list of tuples of terms and doc ids'''
class MyParser():
	def __init__(self, lines):
		self.parse_text(lines)

	def parse_text(self, lines):
		soup = BeautifulSoup(lines,'html.parser')
		result_reuters = soup.find_all('reuters')	#find all similar texts with tags (reuters)
		for item in result_reuters:	# for each text with tags
			item = str(item)	#convert to string to parse further
			text_w_tags = BeautifulSoup(item, 'html.parser')
			doc_id = text_w_tags.reuters['newid']	#find doc id for a text
			pure_text = text_w_tags.get_text()	#get the pure text and normalize
			pure_text = re.sub(r'[\.+,!?:\n"\/]+', ' ', pure_text)
			pure_text = pure_text.lower().replace('\'', ' \'').split()
			i = 0
			for word in pure_text:	#for each word append it to the parsed list
				parsed_list.append((word, int(doc_id), i))
				i +=1
		parsed_list.sort()


class Posting:
	def __init__(self, element):
		self.doc_id = element[1]
		self.append_position(element)

	def append_position(self, element):
		self.length = 0
		self.positions = []
		self.positions.append(element[2])
		self.length = len(self.positions)

	def __repr__(self):
		return str(self.length) + "\t" + str(self.doc_id) + "\t" + str(self.positions)

'''Initially, when Postings List object is created, it has an attribute postings_list
which is empty. It has a function which appends an object of Posting to the list if the id is unique'''
class PostingsList:
	def __init__(self):
		self.length = 0
		self.postings_list = []
		
	def append_posting(self, element):
		self.postings_list.append(Posting(element))
		self.length = len(self.postings_list)

	def add_position(self, element):
		self.postings_list[-1].positions.append(element[2])
		self.postings_list[-1].length = len(self.postings_list[-1].positions)

	def __repr__(self):
		return str(self.length) + "\t" + str(self.postings_list)


class Dictionary:
	def __init__(self, element):
		self.term = element[0]
		self.postlist = PostingsList()

'''The main code is in here'''		
class Index:
	def __init__(self, elements):
		self.make_index(elements)

	def make_index(self, elements):
		self.index = {}
		prev_term = ''
		prev_posting = -456789
		for element in elements:
			if element == elements[0]:
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
			elif element == elements[-1] and element[0] != prev_term:
				self.index[dict_obj.term] = dict_obj.postlist
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
				self.index[dict_obj.term] = dict_obj.postlist
			elif element == elements[-1] and element[0] == prev_term:
				if element[1] == prev_posting:
					dict_obj.postlist.add_position(element)
				else:
					dict_obj.postlist.append_posting(element)
				self.index[dict_obj.term] = dict_obj.postlist
			elif element[0] != prev_term:
				self.index[dict_obj.term] = dict_obj.postlist
				dict_obj = Dictionary(element)
				dict_obj.postlist.append_posting(element)
			else:
				if element[1] == prev_posting:
					dict_obj.postlist.add_position(element)
				else:
					dict_obj.postlist.append_posting(element)


			prev_term = element[0]
			prev_posting = element[1]



'''This class does the query: it will stop only if you say 'exit'. It takes
a query input, normalizes and checks for each word if it's in index,
and then puts the PLs into setlists to do intersection'''
s = set()
class Query:
	def query(self, index_dict):
		while True:
			setlists = []
			your_query = input('Your query:')
			queryTerm = your_query.lower().split(' ')
			ranking_obj = Ranking()
			if your_query == 'exit':
				exit = input('Do you want to quit the IR system?: y/n: ')
				if exit == 'y':
					print("--- %s seconds ---" % (time.time() - start_time))
					sys.exit()
			else:
				tfidf = {}
				tfidf_query = {}
				lists_to_compare = {}
				for term in queryTerm:
					tf_list = [] #to store the tf scores of each term for each document
					tf_q_list = [] #to store the tf scores of each term in query
					if term in index_dict.keys():
						list_of_postings = []
						lists_to_compare[index_dict[term]] = queryTerm.index(term)
						for m in index_dict[term].postings_list:
							list_of_postings.append(m.doc_id)
							tf_list.append(ranking_obj.tf(term, m))
						idf = ranking_obj.idf(term, list_of_postings)
						tf_q_list.append(ranking_obj.tf_query(term, queryTerm))
						tfidf_query[term] = ranking_obj.tf_idf_query(tf_q_list, idf)
						tfidf[term] = ranking_obj.tf_idf(tf_list, idf)
				# print(len(lists_to_compare))
				# print(lists_to_compare)
				for qTerm, index in lists_to_compare.items():
					if index == 0:
						p1 = qTerm
					else:
						p2 = qTerm
						k = index
						phraseQuery_obj = PhraseQuery()
						documents = phraseQuery_obj.phrase_query(p1, p2, k)
						# print(documents)
						setlists.append(set(documents))
			if setlists != [] and len(setlists) > 1:

				s = set.intersection(*setlists)
				if len(s)>=1:
					print('Documents found for the query \'',your_query,'\' are: ', ranking_obj.cos_sim(s, queryTerm, tfidf_query, tfidf))
					# print('Documents found for the query \'',your_query,'\' are: ',s)
					i = 10
					Evaluation().naive_rank(i,your_query,s)
				else:
					print('Sorry. No intersection found.')
			elif len(setlists) == 1:
				print('Documents found for the query \'',your_query,'\' are: ', ranking_obj.cos_sim(s, queryTerm, tfidf_query, tfidf))
				# print('Documents found for the query \'',your_query,'\' are: ', setlists[0])
				i = 10
				Evaluation().naive_rank(i,your_query,setlists[0])

			else:
				print('Sorry. No intersection found')

class Ranking:
    def __init__(self):
        #document lenghts
        self.doc_len = Counter(elem[1] for elem in parsed_list)
        #collection length
        self.col_len = len(self.doc_len)

    def idf(self,term,lst):
        idf_dict = {}
        idf_dict[term] = math.log10(self.col_len/len(lst))
        return idf_dict

    def tf(self,term,post):
        #Posting.lenght = dokumanda kac tane o term'den var #, post.doc_id
        tf_dict = {}
        tf_dict[term] = [post.doc_id, post.length / float(self.doc_len[post.doc_id])]
        return tf_dict

    def tf_query(self,term,query):
        tf_query = {}
        tf_query[term] = 1 / len(query)
        return tf_query

    def tf_idf_query(self,tf_list,idf):
        for tf in tf_list:
            for term,value in tf.items():
                return value*idf[term]

    def tf_idf(self,tf_list,idf):
        tfidf = {}
        for tf in tf_list:
            for term,value in tf.items():
                tfidf[value[0]] = value[1] * idf[term]
        return tfidf

    def cos_sim(self,setlist,queryTerm,tfidf_query,tfidf):
        cos_similarities = {}
        for doc in setlist:
            for term in queryTerm:
                dot_prod=np.dot(tfidf_query[term],tfidf[term][doc])
            cos_similarities[doc] = dot_prod / (len(queryTerm)*self.doc_len[doc])
        sorted_cos = sorted(cos_similarities.items(), key=operator.itemgetter(1))
        i = []
        for x in sorted_cos[0:10]:
            i.append(x[0])
        return i



recall_points = []
precision_points = []
class Evaluation:

	def naive_rank(self,k,query,documents):
		doc = list(documents)
		predicted = []
		sorted = False
		print(doc)
		while not sorted:
			sorted = True
			for i in range(len(doc)-1):
				if doc[i]>doc[i+1] :
					sorted = False
					doc[i] , doc[i+1] = doc[i+1] , doc[i]


		print('The documents found for \'',query,'\'are: ')
		for i in range(len(doc)):
			#predicted.append(doc[i])
			print(doc[i],'\n')


		Evaluation().evaluate(len(doc),query,doc)


	def evaluate(self,k,query,documents):
		#get gold standard
		gold = []
		with open('gold_standard.txt') as fp:
			line = fp.readline()
			count = 1
			while count <= 125:
				if query in line:
					x = line.split(',')
					gold.append(x[1].strip('\n'))
				line = fp.readline()
				count = count + 1

		#plot the PR curve
		file = open('gold_standard.txt')
		file = file.read()
		if query in file:
			Evaluation().metrics(gold,documents)
			Evaluation().plot(gold,documents)
			print("F-score is:")
			Evaluation().unranked_evaluation(precision_points,recall_points)




	def metrics(self,gold,predicted):
		#precision_points = []
		#recall_points = []
		precision_dynamic = []
		gold = [int(i) for i in gold]
		#get points for precision and recall
		#for every new element in precision, calculate recall at that point
		for i in range(0,len(predicted)-1):
			precision_dynamic.append(predicted[i])
			true_positive = [value for value in precision_dynamic if value in gold]
			false_positive = list(set(precision_dynamic).difference(set(gold)))
			false_negative = list(set(gold).difference(set(precision_dynamic)))
			precision_points.append(len(true_positive)/(len(true_positive)+len(false_positive)))
			recall_points.append(len(true_positive)/(len(true_positive)+len(false_negative)))



	def plot(self,gold,predicted):

		#plot points
		p = np.maximum.accumulate(precision_points[::-1])[::-1]
		fig , ax = plt.subplots(1,1)
		#ax.hold(True)
		ax.plot(recall_points,precision_points,'--b')
		ax.step(recall_points,p,'--r')
		#plt.show()


		#THis is the actual plot:
		#plt.plot(recall_points,precision_points)
		#f1 = scipy.interpolate.interp1d(recall_points,p,kind='linear')
		#plt.plot(recall_points,precision_points,'o',p,f1(p),label="interpolate")
		plt.show()







	def unranked_evaluation(self,precision,recall):
		p = sum(precision)/len(precision)
		r = sum(recall)/len(recall)
		F_score = (2*p*r)/(p+r)
		

		pr = precision[-1]
		re = recall[-1]
		F = (2*pr*re)/(pr+re)
		print(F)


parsed_list = []

'''Open the file and parse it in a loop'''
'''Do the indexing and query'''
class Main():
	def __init__(self):
		self.run_index()

	def run_index(self):
		path ='Reuters/'
		files_list = os.listdir(path)
		for file in files_list:
			if file.endswith(".sgm"):
				with open(path + file, "r") as f:
					lines = f.read()
					parser = MyParser(lines)
		# print('parsed list', parsed_list)

		index_obj = Index(parsed_list)
		# print('')
		# print('index', index_obj.index)
		# print('for new', index_obj.index['new'])
		# print('for york', index_obj.index['york'])
		query_obj = Query().query(index_obj.index)
		# print(query_obj.query)


if __name__ == '__main__':
	main_obj = Main()
